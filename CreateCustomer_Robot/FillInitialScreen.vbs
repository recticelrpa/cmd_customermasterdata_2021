'Account group, Customer number, Company code, Sales Organization, Distribution channel, Division
Set objArgs = Wscript.Arguments

If Not IsObject(application) Then
   Set SapGuiAuto  = GetObject("SAPGUI")
   Set application = SapGuiAuto.GetScriptingEngine
End If
If Not IsObject(connection) Then
   Set connection = application.Children(0)
End If
If Not IsObject(session) Then
   Set session    = connection.Children(0)
End If
If IsObject(WScript) Then
   WScript.ConnectObject session,     "on"
   WScript.ConnectObject application, "on"
End If
session.findById("wnd[1]/usr/cmbRF02D-KTOKD").key = objArgs(0)		'Account group
session.findById("wnd[1]/usr/ctxtRF02D-KUNNR").text = objArgs(1)	'Customer number
session.findById("wnd[1]/usr/ctxtRF02D-BUKRS").text = objArgs(2)	'Company code
session.findById("wnd[1]/usr/ctxtRF02D-VKORG").text = objArgs(3)	'Sales Organization
session.findById("wnd[1]/usr/ctxtRF02D-VTWEG").text = objArgs(4)	'Distribution channel
session.findById("wnd[1]/usr/ctxtRF02D-SPART").text = objArgs(5)	'Division
'session.findById("wnd[1]/usr/ctxtRF02D-SPART").setFocus
'session.findById("wnd[1]/usr/ctxtRF02D-SPART").caretPosition = 2
session.findById("wnd[1]/tbar[0]/btn[0]").press
session.findById("wnd[2]/tbar[0]/btn[0]").press
